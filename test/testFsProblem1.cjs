const path = require('path');

const problem1 = require('../fs-problem1.cjs');

const absolutePathOfRandomDirectory = path.resolve(__dirname, 'randomDirectory')

let randomNumberOfFiles = Math.floor(Math.random() * 10) + 1;

problem1(absolutePathOfRandomDirectory, randomNumberOfFiles);

