
/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 

    Ensure that the function is invoked as follows: 
        fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
*/



const fs = require('fs');

function createFiles(randomDirectory, randomNumber, callback) {
    fs.mkdir(randomDirectory, (err) => {
        if (err) {
            callback(err);
        } else {
            console.log("Random directory created");

            let count = 0;
            for (let index = 1; index <= randomNumber; index++) {
                let fileInfo = { file: `${index}` };
                fs.writeFile(`${randomDirectory}/file-${index}.json`, JSON.stringify(fileInfo), (err) => {
                    if (err) {
                        callback(err);
                    } else {
                        console.log(`file-${index}.json is created`);
                        count++;

                        if (count === randomNumber) {
                            console.log("All files are created");
                            callback(null);
                        }
                    }
                });
            }
        }
    });
}

function deleteFiles(randomDirectory, randomNumber, callback) {
    let countDelete = 0;
    for (let index = 1; index <= randomNumber; index++) {
        fs.unlink(`${randomDirectory}/file-${index}.json`, (err) => {
            if (err) {
                callback(err);
            } else {
                console.log(`deleted file-${index}.json`);
                countDelete++;

                if (countDelete === randomNumber) {
                    console.log("deleted all files");
                    fs.rmdir(randomDirectory, (err) => {
                        if (err) {
                            callback(err);
                        } else {
                            console.log("directory deleted");
                            callback(null);
                        }
                    });
                }
            }
        });
    }
}

function problem1(randomDirectory, randomNumber) {
    createFiles(randomDirectory, randomNumber, (err) => {
        if (err) {
            console.error(err);
        } else {
            deleteFiles(randomDirectory, randomNumber, (err) => {
                if (err) {
                    console.error(err);
                }
            });
        }
    });
}

module.exports = problem1;
