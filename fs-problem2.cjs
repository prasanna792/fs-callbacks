/*
   Problem 2:

    Using callbacks and the fs module's asynchronous functions, do the following:
       1. Read the given file lipsum.txt
       2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
       3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
       5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require('fs');
const path = require('path')
const filenamesPath = path.join(__dirname, './filenames.txt')


function problem2() {
    fs.readFile(path.join(__dirname, 'lipsum.txt'), "utf-8", (err, data) => {

        if (err) {
            console.error(err);
        } else {
            console.log("Read lipsum.txt file ")
            let covertToUppercase = data.toString().toUpperCase()  // converting the data into uppercase
            fs.writeFile(path.join(__dirname, 'upperCase.txt'), covertToUppercase, (err, data) => {

                if (err) {
                    console.error(err)
                } else {
                    console.log('coverted lipsum data to  uppercase and stored in a uppercase.txt file')
                    fs.writeFile(filenamesPath, 'upperCase.txt', (err) => {

                        if (err) {
                            console.error(err)
                        } else {
                            console.log('upperCase.txt file name stored in filenames.txt')
                            fs.readFile(path.join(__dirname, 'upperCase.txt'), "utf-8", (err, data) => {

                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log("Read uppercase.txt");

                                    let covertToLowerCase = data.toString().toLowerCase() // converting the data intp lowercase
                                    let splitDataLowerCase = covertToLowerCase.split(". ").join("\n") // converted data splited into sentences
                                    // console.log(splitData, "splitted data");
                                    fs.writeFile(path.join(__dirname, 'lowerCaseSentences.txt'), splitDataLowerCase, (err, data) => {

                                        if (err) {
                                            console.error(err)
                                        } else {
                                            console.log("created lowerCaseSentences.txt file")
                                            fs.appendFile(filenamesPath, ` lowerCaseSentences.txt`, (err, data) => {

                                                if (err) {
                                                    console.error(err)
                                                } else {
                                                    console.log('lowerCaseSentences.txt file name stored in filenames.txt')
                                                    fs.readFile(path.join(__dirname, 'lowerCaseSentences.txt'), "utf-8", (err, data) => {

                                                        if (err) {
                                                            console.error(err)
                                                        } else {
                                                            console.log("Read lowerCaseSentences.txt file ")
                                                            let sortData = data.toString().split("\n").filter(sentences => {
                                                                // console.log(sentences,"sentences")
                                                                if (sentences) {
                                                                    return sentences.trim()
                                                                }
                                                            }).sort().join('\n') // // sort and joins the data
                                                            // console.log(sortData)
                                                            console.log("sorted the data from lowerCaseSentences.txt")
                                                            fs.writeFile(path.join(__dirname, 'sortedData.txt'), sortData, (err) => {

                                                                if (err) {
                                                                    console.error(err)
                                                                } else {
                                                                    console.log("created sortedData.txt file and stored the sorted data")

                                                                    fs.appendFile(filenamesPath, ' sortedData.txt', (err, data) => {
                                                                        if (err) {
                                                                            console.error(err)
                                                                        } else {
                                                                            console.log('sortedData.txt file name stored in filenames.txt')
                                                                            fs.readFile(filenamesPath, "utf-8", (err, data) => {
                                                                                if (err) {
                                                                                    console.error()
                                                                                } else {
                                                                                    console.log("Reading filenames.txt ")
                                                                                    data.toString().split(" ").map(file => {
                                                                                        fs.unlink(path.join(__dirname,`${file}`), (err) => {
                                                                                            if (err) {
                                                                                                console.error(err)
                                                                                            } else {
                                                                                                console.log(`${file} deleted`)
                                                                                                return;
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            })
        }
    })
}

// problem2();

module.exports = problem2;
